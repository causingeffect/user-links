# [User Links](https://bitbucket.org/causingeffect/user-links)

A Craft plugin that gives Users:

 * front-end URLs
 * Rich Text editor links
 * [LinkIt](https://github.com/fruitstudios/LinkIt) support (optional)



## Setup


### Step 1: Set up the Preparse Plugin

Make sure you have set up and installed the [Preparse plugin](https://github.com/aelvan/Preparse-Field-Craft)


### Step 2: Set up the fields

Since users do not have URLs or slugs, we'll need to give them equivalents using Preparse fields.

Create a Preparse field with the name `User URL` and the handle `userUrl`. Use the following for the `Twig code to parse`:

```twig
{%- spaceless -%}
	{#- output the detailPagePath (the relateive part of the URL from the config) -#}
	{%- set detailPagePath = craft.config.get('detailPagePath', 'userlinks') ?? '' -%}
	{%- set detailPagePath = '/'~detailPagePath|trim('/')~'/' -%}
	{{- detailPagePath|replace({'//':'/'}) -}}

	{#- output the user's kebabbed name -#}
	{{- user.fullName|kebab -}}

	{#- if there are other users with the same name, then we need to append the user id -#}
	{%- set users = craft.users.firstName(user.firstName).lastName(user.lastName).find() -%}
	{%- if users|length > 1 -%}
		-{{- user.id -}}
	{%- endif -%}
{%- endspaceless -%}
```

Now let's create another Preparse field with the name `User Slug` and the handle `userSlug`. Use the following for the `Twig code to parse`:

```twig
{%- spaceless -%}
	{#- output the user's kebabbed name -#}
	{{- user.fullName|kebab -}}

	{#- if there are other users with the same name, then we need to append the user id -#}
	{%- set users = craft.users.firstName(user.firstName).lastName(user.lastName).find() -%}
	{%- if users|length > 1 -%}
		-{{- user.id -}}
	{%- endif -%}
{%- endspaceless -%}
```
 
 Now let's assign those fields to our Users. Go to Settings -> Users -> Fields, drag the two fields to the tab and save.

### Step 3: Add the User Links config file

Create a file named `userlinks.php` in the `craft/config` directory, and copy the following code into it:
```php
<?php

/**
 * Config file for the User Links plugin.
 *
 * Called in a template by: `craft.config.get('{key}', 'userlinks')`
 * Called in PHP by: `craft()->config->get('{key}', 'userlinks');`
 */
return [
	/**
	 * The relative URL to the detail page. Example: 'person'
	 */
	'detailPagePath' => '',

	/**
	 * Optional: The handles of the user groups to limit the
	 * LinkIt user and rich text fields to. Ex: ['siteMembers', 'siteStaff']
	 * Leave blank for all groups to be included
	 */
	'limitToUserGroups' => [],

	/**
	 * Optional: The handles of the user groups that should have a
	 * Link control panel field. Ex: ['siteMembers', 'siteStaff'] 
	 * Leave blank for all groups to be included.
	 */
	'cpLinkToUserGroups' => [],

	/**
	 * Optional: The field to use for the text in the control panel links. 
	 * Will default to the respective user's fullName if left empty.
	 */
	'userTextAttribute' => '',
];
```


### Step 4: Populate the config file

Customize the config file from the previous step. Set the `detailPagePath` to the relative URL path for the user URLs. For now, let's assume you have set `'detailPagePath' => 'person',`. That means that your users will end up having URLs (from the `userUrl` field) that look like something like `person/firstname-lastname`.

If you want to specify user group handles for the `limitToUserGroups` and `cpLinkToUserGroups` arrays, you can find the user group handles in Settings -> User Settings -> User Groups.


### Step 5: Set up a template

Let's set up a template for our new user detail pages. Let's go ahead and make a template called `_users/user.twig`

```twig
{% set user = craft.users.userSlug(userSlug).first() %}
{% if user is empty %}
	{% exit 404 %}
{% endif %}

<h1>{{ user.fullName }}</h1>
<p>This user's link: <a href="{{ user.userUrl }}">{{ user.userUrl }}</a></p>
<p>This user's slug: <a href="{{ user.userSlug }}">{{ user.userSlug }}</a></p>
```


### Step 6: Set up routing

Our users now have URLs and a template. We need to create a route so Craft knows to send those URLs to that template.

In `craft/routes.php`, let's add a route to connect our users' URLs to our template:

```php
	return array(
		'person/(?P<userSlug>[^/]+)' => '_users/user',
	);
```

Now our template will receive a `userSlug` that will contain the user's unique slug.


### Step 7: Install the User Links plugin

* Copy this plugin to the `craft/plugin` directory and install it.
* Log into the Craft control panel and go to Settings -> Plugins
* Locate the User Links plugin in the list and click on "Install"


### Step 8: Re-save your existing users

The Preparse fields will only have data for users that have been saved after the fields were added, so all of the users will need to be saved once for their new link and slug fields to be populated. If you have a lot of users already in your site, I find that the easiest way to batch save users in Craft is by using the [Walk plugin](https://github.com/TopShelfCraft/Walk). Simply install the plugin and then run the following command from your command line: `php yiic walk users users.saveUser --limit=null`. If you have a lot of users, like more than 1,000, you should probably run that command as a task: `php yiic walk users users.saveUser --limit=null --asTask`


### Step 9: (Optional) Set up links for the users in the control panel

In the Craft control panel, click on `Users`. In the button of the left hand nav, where all of the user groups are listed out, there is a little cog icon. Click on that cog icon and a modal will reveal a list of field checkboxes to customize the user listings table for each group. There is now a `Link` field option. You can check that checkbox on each user group to have the users' Link to appear.

Note: The Link will only display for each user if the `cpLinkToUserGroups` is left empty or if the user belongs in one of the user groups listed in that setting.


### Step 10: (Optional) Add the ability to link to users in Redactor (Rich Text) fields

The User Links plugin provides the ability to link to users from Rich Text fields. In order to set this up, you'll need to add the `userlinks` Redactor plugin to each Redactor config file you want to use it in. The Craft Redactor config files are found in `craft/config/redactor`. Add `"userlinks"` to the `"plugins"` array of each config you would like set up. For example, you may have a `Standard.json` Redactor config file that looks like this:

```json
{
	"buttons": ["format","bold","italic","lists","link","file","horizontalrule"],
	"plugins": ["source","fullscreen","userlinks"]
}
```


## Contributions

Pull requests welcome! Please make pull requests to the `dev` branch.