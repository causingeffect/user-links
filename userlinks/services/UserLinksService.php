<?php
/**
 * User Links plugin for Craft CMS
 *
 * User Links Service
 *
 * usage: craft()->userLinks->methodName()
 *
 * @author    Aaron Waldon
 * @copyright Copyright (c) 2017 Aaron Waldon
 * @link      https://www.causingeffect.com
 * @package   User Links
 * @since     1.0.0
 */

namespace Craft;

class UserLinksService extends BaseApplicationComponent
{
	/**
	 * Get the user groups.
	 *
	 * @return array
	 */
	public function getUserGroups()
	{
		$groups = [];
		$groupModels = craft()->userGroups->getAllGroups();

		if (!empty($groupModels))
		{
			$groups[] = [
				'label' => Craft::t('All Users'),
				'value' => '*'
			];
		}

		foreach ($groupModels as $groupModel)
		{
			$groups[] = [
				'label' => Craft::t($groupModel->name),
				'value' => 'group:'.$groupModel->id
			];
		}

		return $groups;
	}

	/**
	 *
	 *
	 * @return array
	 */
	public function getSources()
	{
		$sources = [];
		$groupModels = craft()->userGroups->getAllGroups();
		$limitedGroups = $this->getLimitedUserGroups();

		foreach ($groupModels as $groupModel)
		{
			if (empty($limitedGroups) || in_array($groupModel->handle ,$limitedGroups))
			{
				$sources[] = 'group:'.$groupModel->id;
			}
		}

		return $sources;
	}

	/**
	 * Get the User.
	 *
	 * @param $type
	 * @param $id
	 * @return array|bool
	 */
	public function getUser($type, $id)
	{
		if (empty($type) || empty($id))
		{
			return false;
		}

		if ($type !== 'User')
		{
			return false;
		}

		//get the user text attribute to use
		$userTextAttribute = craft()->config->get('userTextAttribute', 'userlinks');

		$user = craft()->users->getUserById($id);
		if ($user)
		{
			$url = $this->getUserUrl($user);

			return [
				'url'     => $url,
				'text'    => empty($userTextAttribute) ? $user->getFullName() : $user->{$userTextAttribute},
				'element' => $user
			];
		}

		return false;
	}

	/**
	 * Returns the user URL.
	 *
	 * @param $user
	 * @return string
	 */
	public function getUserUrl($user)
	{
		$detailPage = craft()->config->get('detailPagePath', 'userlinks');
		$detailPage = !empty($detailPage) ? '/'.trim($detailPage, '/') : '';

		return $detailPage.'/'.$user->userSlug;
	}

	/*
	 * Returns an array of the user handles to limit the user groups to, if any.
	 *
	 * @return array
	 */
	public function getLimitedUserGroups() {
		$limitedGroups = craft()->config->get('limitToUserGroups', 'userlinks');
		$limitedGroups = empty($limitedGroups) ? array() : (array) $limitedGroups;

		return $limitedGroups;
	}

	/**
	 * Returns an array of the user group handles that can be linked to in the cp.
	 *
	 * @return array
	 */
	public function getCpLinkUserGroups() {
		$linkGroups = craft()->config->get('cpLinkToUserGroups', 'userlinks');
		$linkGroups = empty($linkGroups) ? array() : (array) $linkGroups;

		return $linkGroups;
	}

	/**
	 * Determines whether or not the user is in one of the groups
	 * that will display a link to their user profile in the cp.
	 *
	 * @param UserModel $user
	 * @return bool
	 */
	public function isUserInLinkGroup(UserModel $user)
	{
		$linkGroups = $this->getCpLinkUserGroups();
		if (empty($linkGroups))
		{
			return false;
		}

		foreach ($linkGroups as $linkGroup)
		{
			if ($user->isInGroup($linkGroup))
			{
				return true;
			}
		}

		return false;
	}
}