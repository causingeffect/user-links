<?php
namespace Craft;

class UserLinksPlugin extends BasePlugin
{
	/**
	 * The plugin name.
	 *
	 * @return string
	 */
	public function getName()
	{
		return Craft::t('User Links');
	}

	/**
	 * The plugin version.
	 *
	 * @return string
	 */
	public function getVersion()
	{
		return '1.0.0';
	}

	/**
	 * The release feed URL.
	 *
	 * @return string
	 */
	public function getReleaseFeedUrl()
	{
		return 'https://bitbucket.org/causingeffect/user-links/raw/master/releases.json';
	}

	/**
	 * Returns the developer name.
	 *
	 * @return string
	 */
	public function getDeveloper()
	{
		return 'Causing Effect';
	}

	/**
	 * Returns the developer URL.
	 *
	 * @return string
	 */
	public function getDeveloperUrl()
	{
		return 'https://www.causingeffect.com';
	}

	/**
	 * Returns the plugin description.
	 *
	 * @return null|string
	 */
	public function getDescription()
	{
		return 'Makes users linkable.';
	}

	/**
	 * Returns the documentation URL.
	 *
	 * @return null|string
	 */
	public function getDocumentationUrl()
	{
		return 'https://bitbucket.org/causingeffect/user-links';
	}

	/**
	 * Adds the Redactor plugin to cp pages.
	 */
	public function init()
	{
		if (!craft()->isConsole()) {
			if (craft()->request->isCpRequest() && craft()->userSession->isLoggedIn()) {
				craft()->templates->includeJsResource('userlinks/js/userlinks.js');
			}
		}
	}

	/**
	 * Registers the linkIt User element type.
	 *
	 * @return array
	 */
	public function linkit_registerElementTypes()
	{
		return [
			'User' => [
				'name'                   => Craft::t('User'),
				'pluralName'             => Craft::t('Users'),
				'selectionLabelDefault'  => Craft::t('Select a User'),
				'emptyInputErrorMessage' => Craft::t('Please select a user'),
				'elementType'            => ElementType::User,
				'sources' => craft()->userLinks->getUserGroups()
			]
        ];
	}

	/**
	 * Returns the linkIt array for the specified User.
	 *
	 * @param  string $type
	 * @param  int    $id
	 * @return array|false
	 */
	public function linkit_getElementData($type, $id)
	{
		return craft()->userLinks->getUser($type, $id);
	}

	/**
	 * Adds the ability to link to Users from Redactor.
	 *
	 * @see https://craftcms.com/docs/plugins/hooks-reference#addRichTextLinkOptions
	 * @return array
	 */
	public function addRichTextLinkOptions()
	{
		$sources = craft()->userLinks->getSources();

		return array(
			array(
				'optionTitle' => Craft::t('Link to a user'),
				'elementType' => 'User',
				'sources' => $sources
			),
		);
	}

	/**
	 * Adds additional table attributes. Sets up a "Link" field for the Users table.
	 *
	 * @see https://craftcms.com/docs/plugins/hooks-reference#defineAdditionalUserTableAttributes
	 * @return array
	 */
	public function defineAdditionalUserTableAttributes()
	{
		return array('link' => "Link");
	}

	/**
	 * Adds the ability to define additional table HTML. Sets up the "Link" icon for the Users table.
	 *
	 * @see https://craftcms.com/docs/plugins/hooks-reference#getUserTableAttributeHtml
	 * @param UserModel $user
	 * @param $attribute
	 * @return null|string
	 */
	public function getUserTableAttributeHtml(UserModel $user, $attribute)
	{
		if ($attribute=='link')
		{
			if ($user->userUrl && craft()->userLinks->isUserInLinkGroup($user))
			{
				return '<a href="'.$user->userUrl.'" target="_blank" data-icon="world" title="Visit webpage"></a>';
			}
			return '';
		}
		return null;
	}
}