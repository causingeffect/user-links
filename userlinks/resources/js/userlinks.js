if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.userlinks = function() {
	return {
		init: function() {
			this.userlinks.replaceReferences();
			this.core.element().on('change.callback.redactor', this.userlinks.replaceReferences);
		},
		replaceReferences: function() {
			var $userLinks = this.$editor.find('a[href^="#user:"]');
			if (!$userLinks.length) {
				return;
			}
			var regex = /#(user:\d+)/;
			var isDirty = false;
			$.each($userLinks, function(index,ele) {
				//make sure this is not already a reference tag
				if (ele.href.indexOf(':userUrl') !== -1) {
					return;
				}

				var match = regex.exec(ele.href);
				if (!match) {
					return;
				}
				isDirty = true;

				//update the href
				ele.href = '{'+match[1]+':userUrl}';
			});

			if (isDirty) {
				this.code.sync();
			}
		}
	};
};